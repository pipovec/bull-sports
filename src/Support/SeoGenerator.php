<?php declare(strict_types=1);


namespace App\Support;


use App\Entity\Competition;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\String\UnicodeString;

/**
 * @author Boris Fekar
 * @createdAt 24. 9. 2021
 * @package  App\Support
 */
class SeoGenerator
{
    /**
     * @param string|null $image
     * @param string|null $url
     * @return array
     */
    public function seoGenerally(string $title, string $description, string $image = null, string $url = null): array
    {
        $description = new UnicodeString(strip_tags($description));

        $seo = [
            'og:site_name' => 'Bull sports',
            'og:type'=> 'article',
            'title' => $title,
            'description' => $description->truncate(158, '...'),
            'og:title' => $title,
            'og:description' => $description->truncate(158, '...'),
        ];


        if ($image) {
            $seo['og:image'] = $image;
        }

        if ($url) {
            $seo['og:url'] = $url;
        }

        return $seo;
    }


    /**
     * Create seo tags for the sport discipline
     *
     * @return array
     */
    public function seoCompetition(Competition $competition): array
    {
        $description = new UnicodeString(strip_tags((string) $competition->getSeoDescription()));
        $title = new UnicodeString($competition->getName() . " - " . $competition->getNameSk());
        $image = $competition->getImage()->getAWSname();

        return [
            'og:site_name' => 'Bull sports',
            'og:type'=> 'article',
            'og:url' => 'https://bull_sports/',
            'title' => $title,
            'description' => $description->truncate(158, '...'),
            'og:title' => $title,
            'og:description' => $description->truncate(158, '...'),
            'og:image' => $image,
        ];
    }
}