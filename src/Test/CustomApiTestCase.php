<?php declare(strict_types=1);


namespace App\Test;


use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use App\Entity\User;
use Doctrine\ORM\EntityManager;
use App\Tests\DataSource\DataSource;

/**
 * @author Boris Fekar
 * @createdAt 4. 9. 2021
 * @package  App\Test
 */
class CustomApiTestCase extends ApiTestCase
{

    public const ADMIN_API_TOKEN = '$2y$13$TdwvHZdAhf3A.JvLar6GN.Uf5kzduMf2dyOzYjUCBsAQ7IyyKWpPO';
    public const ADMIN_PASSWORD = '$2y$13$TdwvHZdAhf3A.JvLar6GN.Uf5kzduMf2dyOzYjUCBsAQ7IyyKWpPO';

    private EntityManager $entityManager;

    protected DataSource $dataSource;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->dataSource = new DataSource();
    }

    protected function createUser(
        string $email,
        string $name,
        string $surname,
        string $password,
        array $roles,
        string $apiToken,

    ): string
    {
        $user = new User();
        $user->setEmail($email);
        $user->setName($name);
        $user->setSurname($surname);
        $user->setRoles($roles);
        $user->setPassword($password);
        $user->setApiToken($apiToken);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return "/api/users/".$user->getId();
    }


    protected function createUserIri(): string
    {
        return $this->createUser(
            'boris@admin.com',
            'Boris',
            'Admin',
            self::ADMIN_PASSWORD,
            ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_ORGANIZER'],
            self::ADMIN_API_TOKEN
        );
    }

    protected function loginUser(Client $client, $user)
    {
        if ($user instanceof User) {
            $client->request('POST', '/login', [
                'json' => [
                    'email' => $user->getEmail(),
                    'password' => $user->getPassword()
                ]
            ]);

            $this->assertResponseStatusCodeSame(201);
        }
    }
}