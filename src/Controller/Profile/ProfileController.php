<?php

namespace App\Controller\Profile;



use ApiPlatform\Api\IriConverterInterface;
use App\Controller\BaseController;
use App\Repository\BreedsRepository;
use App\Repository\DogsRepository;
use App\Repository\UsersRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/profile', name: 'profile_')]
class ProfileController extends BaseController
{
    public function __construct(
        protected UsersRepository $usersRepository,
        protected SerializerInterface $serializer,
        protected IriConverterInterface $iriConverter,
        protected DogsRepository $dogsRepository,
        protected BreedsRepository $breedsRepository,
    ){}

    #[Route('/', name: 'index')]
    #[Route('/uprav-profil')]
    #[Route('/moji-psy/')]
    #[Route('/moje-akcie/')]
    #[Route('/trening')]
    public function index(): Response
    {
        return $this->render('profile/profile/index.html.twig', [
            'aws' => $this->getParameter('aws'),
            'menu_items' => $this->getMenu(),
            'apiToken' => $this->getUser()->getApiToken(),
            'userIRI' => $this->iriConverter->getIriFromResource($this->getUser()),
        ]);
    }
}
