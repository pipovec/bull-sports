<?php declare(strict_types=1);


namespace App\Controller\Competitions;


use App\Controller\BaseController;
use App\Repository\CompetitionRepository;
use App\Support\SeoGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class CompetitionsController extends BaseController
{
    public function __construct(private readonly CompetitionRepository $competitionRepository, private readonly SeoGenerator $seoGenerator)
    {
    }

    #[Route('/discipliny', name: 'competitions', methods: ['GET'])]
    public function index(): Response
    {
        $competitions = $this->competitionRepository->findAll();
        foreach($competitions as $competition) {
            $competition->path = $this->generateUrl(
                'competition',
                [
                    'id' => $competition->getId(),
                    'name' =>  $this->normalizeNameUrl($competition->getNameSk()),
                ]
            );
        }

        return $this->render('competitions/competitions.html.twig', [
            'aws' => $this->getParameter('aws'),
            'menu_items' => $this->getMenu(),
            'competitions' => $competitions,
        ]);
    }

    #[Route('/disciplina/{id}/{name}/', name: 'competition',  methods: ['GET']),  ]
    public function competition(string $id, string $name = null): Response
    {
        $name = str_replace('-', ' ', (string) $name);
        $competition = $this->competitionRepository->find($id);


        return $this->render('competitions/competition.html.twig', [
            'menu_items' => $this->getMenu(),
            'competition' => $competition,
            'seo' => $this->seoGenerator->seoCompetition($competition),
            'aws' => $this->getParameter('aws'),
        ]);
    }


}
