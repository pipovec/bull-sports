<?php

namespace App\Controller\Security;

use App\Controller\BaseController;
use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class RegistrationController
 *
 * @package App\Controller\Security
 */

class RegistrationController extends BaseController
{
    #[Route('/register', name: 'app_register', methods: ['GET', 'POST'])]
    public function register(
        Request $request,
        UserPasswordHasherInterface $passwordEncoder,
        ValidatorInterface $validator,
        ManagerRegistry $managerRegistry
    ): Response
    {

        $submittedToken = $request->request->get('token');

        $formErrors = [];
        $error = null;

        if ($this->isCsrfTokenValid('register-form', $submittedToken)) {
            $emailValidator = $this->emailValidator();
//            $formErrors[] = $validator->validate($request->request->get('email'), $emailValidator);

            if ($request->request->get('password') !== $request->request->get('password_again')) {
                $formErrors['password'] = "Hesla sa nezhoduju, prosim skontrolujte si to.";
            } else {

                $user = new User();
                $user->setEmail($request->request->get('email'));
                $user->setPassword($passwordEncoder->hashPassword($user, $request->request->get('password')));

                $user->setApiToken($passwordEncoder->hashPassword(
                    $user,
                    $request->request->get('surname') ." ".
                    $request->request->get('name'))
                );

                $user->setName($request->request->get('name'));
                if ($request->request->has('surname')) {
                    $user->setSurname($request->request->get('surname'));
                }

                $user->setRoles(['ROLE_USER']);
                $user->setCreatedAt();
            }

            if( count($formErrors) === 0 ) {
                try{

                    $entityManager = $managerRegistry->getManager();
                    $entityManager->persist($user);
                    $entityManager->flush();

                    return $this->redirectToRoute('app_login');
                }catch(\Exception $e)  {
                    $error = $e->getMessage();
                }
            }
        }

        return $this->render(
            'registration/register.html.twig',
            [
                'formErrors' => $formErrors,
                'error' => $error,
                'formData' => $request->request->all(),
            ]
        );
    }

    /**
     * Validate email
     *
     * @return Email
     */
    public function emailValidator(): Email
    {
        return new Email([], 'Nespravny format emailu, prosim skontrolujte si ho');
    }
}
