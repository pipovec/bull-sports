<?php declare(strict_types=1);


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @author Boris Fekar
 * @createdAt 6. 9. 2021
 * @package  App\Controller
 */
abstract class BaseController extends AbstractController
{
    /**
     * @return array
     */
    protected function getMenu(): array
    {
        $mainMenu = [
            'Domov'    => $this->generateUrl('welcome'),
//            'Športové akcie'    => $this->generateUrl('sport_events'),
//            'Výsledky' => "#",
            'Disciplíny' => $this->generateUrl('competitions'),
        ];

        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $mainMenu['Profil'] = $this->generateUrl('profile_index');

            if($this->isGranted('ROLE_ADMIN'))
                $mainMenu['Administrácia'] = $this->generateUrl('admin_home');
                $mainMenu['CMS'] = $this->generateUrl('cms');

            $mainMenu['Odhlásenie'] = $this->generateUrl('app_logout');
        } else {
            $mainMenu['Login'] = $this->generateUrl('app_login');
            $mainMenu['Registrácia'] = $this->generateUrl('app_register');
        }

        return $mainMenu;
    }

    /**
     * Changes competition name to lower case and snake
     *
     * @param $name
     * @return array|string
     */
    protected function normalizeNameUrl($name): array|string
    {
        $name = strtolower((string) $name);
        return str_replace(' ', '-', $name);
    }
}
