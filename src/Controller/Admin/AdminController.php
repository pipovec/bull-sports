<?php

namespace App\Controller\Admin;

use ApiPlatform\Api\IriConverterInterface;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;


#[Route('/admin', name: 'admin_')]
class AdminController extends BaseController
{
    public function __construct(
        protected IriConverterInterface $iriConverter,
    ) {}

    #[Route('/', name: 'index')]
    #[Route('/uzivatelia', name: 'users')]
    #[Route('/akcie', name: 'events')]
    #[Route('/domov', name: 'home')]
    #[Route('/discipliny', name: 'competitions')]
    #[Route('/discipliny/{competionId}', name: 'competitions_id')]
    #[Route('/discipliny/{competionId}/{slug}', name: 'competitions_id_slug')]
    #[Route('/plemena', name: 'breeds')]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        return $this->render('admin/admin/index.html.twig', [
            'aws' => $this->getParameter('aws'),
            'menu_items' => $this->getMenu(),
            'apiToken' => $this->getUser()->getApiToken(),
            'userIRI' => $this->iriConverter->getIriFromResource($this->getUser()),
        ]);
    }
}
