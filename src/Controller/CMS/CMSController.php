<?php declare(strict_types=1);


namespace App\Controller\CMS;


use ApiPlatform\Api\IriConverterInterface;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;


class CMSController extends BaseController
{
    public function __construct(protected IriConverterInterface $iriConverter)
    {
    }

    #[Route('/cms', name: 'cms')]
    #[Route('/cms/clanky', name: 'cms_articles')]
    #[Route('/cms/clanky/pridat', name: 'cms_article_add')]
    public function index(): Response
    {
        return $this->render('cms/index.html.twig', [
                'aws' => $this->getParameter('aws'),
                'menu_items' => $this->getMenu(),
                'apiToken' => $this->getUser()->getApiToken(),
                'userIRI' => $this->iriConverter->getIriFromResource($this->getUser()),
            ]
        );
    }
}
