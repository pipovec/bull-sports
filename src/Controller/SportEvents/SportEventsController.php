<?php

namespace App\Controller\SportEvents;

use App\Controller\BaseController;
use App\Repository\EventsRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class SportEventsController extends  BaseController
{
    public function __construct(private readonly EventsRepository $eventsRepository){}

    #[Route(path:[
        'sk' => '/sportove-udalosti',
        'en' => '/sport-events'
    ], name: 'sport_events')]
    public function index(): Response
    {
        $sportEvents = $this->eventsRepository->showActiveSportEvents();

        return $this->render('sport_events/index.html.twig', [
            'menu_items' => $this->getMenu(),
            'sport_events' => $sportEvents,
        ]);
    }
}
