<?php declare(strict_types=1);


namespace App\Services\impl;


use App\Services\AWSClientInterface;
use App\Services\ImageProcessInterface;
use App\Services\ResizeAwsImageInterface;
use Aws\Exception\AwsException;
use Aws\Result;
use Aws\S3\S3Client;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @category  Category
 * @package   App\Services\impl
 * @author    Boris Fekar <boris@fekar.net>
 * @createdAt 16. 7. 2022
 * @licence   GNU General Public License
 * @link      <https://opensource.org/licenses/GPL-3.0>
 */
class ImageProcessService implements ImageProcessInterface
{
    private S3Client $s3Client;

    public function __construct(
        private readonly AWSClientInterface $AWSClient,
        private readonly ResizeAwsImageInterface $resizeAwsImage,
        private readonly array $imageSizeVersions
    )
    {
    }

    /**
     * @param string $awsImageName
     * @param string $mimeType
     * @return void
     */
    public function processImage(string $awsImageName, string $mimeType)
    {
        $this->getClient();

        $awsObject = null;

        foreach ($this->imageSizeVersions as $sizeName => $size) {

            $imageNameVersion = $this->createImageNameVersion($sizeName, $awsImageName);

            $result = $this->getImageObject($imageNameVersion);

            if ($result instanceof Result) {
                echo $imageNameVersion . ' exists';
            } else {
                if (!$awsObject) {
                    $awsObject = $this->getImageObject($awsImageName);
                }

                $this->resizeAwsImage
                    ->setAwsObject($awsObject)
                    ->setImageSizeVersion($size)
                    ->resize();

                dd($this->resizeAwsImage);
            }


        }



        dd($awsObject);
    }

    /**
     * @return Result|String
     */
    protected function getImageObject(string $fullImageName): Result|String
    {
        try {
            return $this->s3Client->getObject([
                'Bucket' => 'bull-sports',
                'Key' => $fullImageName,
            ]);
        } catch (AwsException $awsException) {
            return $awsException->getAwsErrorCode();
        }
    }

    /**
     * @return void
     */
    private function getClient(): void
    {
        $this->s3Client = $this->AWSClient->getS3Client([
            'profile' => 'bull-sports',
            'version' => 'latest',
            'region' => 'eu-central-1',
        ]);
    }

    private function createImageNameVersion(
        string $sizeName,
        string $awsImageName
    ): string
    {
        $originalName = explode('.', $awsImageName);
        $count = count($originalName);

        $originalName[$count] = $originalName[$count - 1];
        $originalName[$count-1] = $sizeName;

        return implode('.', $originalName);
    }
}
