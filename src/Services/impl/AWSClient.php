<?php declare(strict_types=1);


namespace App\Services\impl;


use App\Services\AWSClientInterface;
use Aws\S3\S3Client;
use Aws\Sdk;

/**
 * @category  Category
 * @package   App\Services\impl
 * @author    Boris Fekar <boris@fekar.net>
 * @createdAt 16. 7. 2022
 * @licence   GNU General Public License
 * @link      <https://opensource.org/licenses/GPL-3.0>
 */
class AWSClient implements AWSClientInterface
{
    public function __construct(private readonly Sdk $sdk)
    {
    }

    public function getS3Client(array $awsParams): S3Client
    {
        return $this->sdk->createS3($awsParams);
    }
}
