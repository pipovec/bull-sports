<?php declare(strict_types=1);


namespace App\Services\impl;


use App\Services\ResizeAwsImageInterface;
use Aws\Result;
use GuzzleHttp\Psr7\Stream;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @category  Category
 * @package   App\Services\impl
 * @author    Boris Fekar <boris@fekar.net>
 * @createdAt 17. 7. 2022
 * @licence   GNU General Public License
 * @link      <https://opensource.org/licenses/GPL-3.0>
 */
class ResizeAwsImageService implements ResizeAwsImageInterface
{
    public const TEMP_FILE = './var/temp/image_resize.tmp';

    private Result $awsObject;

    protected array $imageSizeVersion;

    public function __construct(private readonly Filesystem $filesystem)
    {
    }

    public function setAwsObject(Result $awsObject): self
    {
        $this->awsObject =  $awsObject;
        return $this;
    }

    public function setImageSizeVersion(array $imageSizeVersion): self
    {
        $this->imageSizeVersion = $imageSizeVersion;
        return $this;
    }

    public function resize(): never
    {
        /** @var Stream $stream */
        $stream = $this->awsObject['Body'];
        $this->saveStream($stream);

        $image = getimagesize(self::TEMP_FILE);
        dd($image);
    }

    private function saveStream(Stream $stream)
    {
        if (!$this->filesystem->exists(self::TEMP_FILE)) {
            $this->filesystem->touch(self::TEMP_FILE);
        }

        file_put_contents(self::TEMP_FILE, $stream->getContents());
    }
}
