<?php

namespace App\Services;

interface ImageProcessInterface
{
    public function processImage(string $awsImageName, string $mimeType);
}
