<?php

namespace App\Services;

use Aws\Result;

interface ResizeAwsImageInterface
{
    public function setAwsObject(Result $awsObject): self;

    public function setImageSizeVersion(array $imageSizeVersion): self;
}
