<?php

namespace App\Services;

use Aws\S3\S3Client;

interface AWSClientInterface
{
    public function getS3Client(array $awsParams): S3Client;
}
