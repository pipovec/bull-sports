<?php

namespace App\Entity;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\BreedsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[ApiResource(
    operations: [
        new Get(),
        new Put(security: 'is_granted("ROLE_ADMIN")'),
        new GetCollection(),
        new Post(security: 'is_granted("ROLE_ADMIN")')],
    normalizationContext: ['groups' => ['breeds:read']],
    denormalizationContext: ['groups' => ['breeds:write']]
)]
#[ORM\Entity(repositoryClass: BreedsRepository::class)]
#[ORM\Table(name: 'breeds')]
class Breed
{
    public function __construct()
    {
        $this->id = Uuid::v4();
    }

    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[Groups(['breeds:read'])]
    private Uuid $id;

    #[Groups(['breeds:read', 'breeds:write'])]
    #[ORM\Column(type: 'string', length: 50)]
    private ?string $breed = null;

    #[Groups(['breeds:read', 'breeds:write'])]
    #[ORM\Column(type: 'text')]
    private ?string $description = null;

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getBreed(): ?string
    {
        return $this->breed;
    }

    public function setBreed(string $breed): self
    {
        $this->breed = $breed;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }
}
