<?php declare (strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\DogsRepository;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Context;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    normalizationContext: [
        'groups' => ['dogs:read'],
        'datetime_format' => 'Y-m-d'
    ],
    denormalizationContext: [
        'groups' => ['dogs:write']
    ],
    security: 'is_granted("ROLE_USER")'
)]
#[Get(security: 'object.owner == user')]
#[Put(security: 'object.owner == user')]
#[Delete(security: 'object.owner == user')]
#[GetCollection(security: 'is_granted("ROLE_ADMIN") or object.owner == user')]
#[Post(security: 'is_granted("ROLE_USER")')]
#[ApiFilter(SearchFilter::class, properties: ['owner' => 'exact'])]
#[ORM\Entity(repositoryClass: DogsRepository::class)]
#[ORM\Table(name: 'dogs')]
class Dog
{
    public function __construct()
    {
        $this->createdAt = new DateTimeImmutable('now', new DateTimeZone('Europe/Bratislava'));
        $this->id = Uuid::v4();
        $this->aframes = new ArrayCollection();
        $this->treadmills = new ArrayCollection();
    }

    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[Groups(['dogs:read'])]
    private Uuid $id;

    #[Groups(['dogs:read', 'dogs:write'])]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'dogs')]
    #[Assert\NotBlank]
    public User $owner;

    #[Groups(['dogs:read', 'dogs:write', 'aframe:read', 'treadmill:read'])]
    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    #[Groups(['dogs:read', 'dogs:write'])]
    #[ORM\Column(type: 'float')]
    private float $weight;

    #[Groups(['dogs:read', 'dogs:write'])]
    #[ORM\Column(type: 'float', nullable: true)]
    private ?float $competition_weight = null;

    #[Groups(['dogs:read', 'dogs:write'])]
    #[ORM\Column(type: 'date')]
    private ?\DateTimeInterface $birthdate = null;

    #[Groups(['dogs:read'])]
    #[Context(normalizationContext: [DateTimeNormalizer::FORMAT_KEY => 'Y-m-d H:i:s'])]
    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeImmutable $createdAt;

    #[Groups(['dogs:read', 'dogs:write'])]
    #[ORM\ManyToOne(targetEntity: Breed::class)]
    #[ORM\JoinColumn(nullable: false)]
    private ?Breed $breed = null;

    #[Groups(['dogs:read', 'dogs:write'])]
    #[ORM\OneToOne(targetEntity: Image::class, cascade: ['persist', 'remove', 'detach'], orphanRemoval: true)]
    private ?Image $image = null;

    #[ORM\OneToMany(mappedBy: 'dog', targetEntity: Aframe::class, orphanRemoval: true)]
    private Collection $aframes;

    #[ORM\OneToMany(mappedBy: 'dog', targetEntity: Treadmill::class, orphanRemoval: true)]
    private Collection $treadmills;

    public function getId() : Uuid
    {
        return $this->id;
    }
    public function setId() : self
    {
        $this->id = Uuid::v4();
        return $this;
    }
    public function getOwner() : User
    {
        return $this->owner;
    }
    public function setOwner(User $owner) : self
    {
        $this->owner = $owner;
        return $this;
    }
    public function getName() : ?string
    {
        return $this->name;
    }
    public function setName(string $name) : self
    {
        $this->name = $name;
        return $this;
    }
    public function getWeight() : ?float
    {
        return $this->weight;
    }
    public function setWeight(float $weight) : self
    {
        $this->weight = $weight;
        return $this;
    }
    public function getCompetitionWeight() : ?float
    {
        return $this->competition_weight;
    }
    public function setCompetitionWeight(?float $competition_weight) : self
    {
        $this->competition_weight = $competition_weight;
        return $this;
    }
    public function getBirthdate() : ?\DateTimeInterface
    {
        return $this->birthdate;
    }
    public function setBirthdate(\DateTimeInterface $birthdate) : self
    {
        $this->birthdate = $birthdate;
        return $this;
    }
    public function getCreatedAt() : ?DateTimeImmutable
    {
        return $this->createdAt;
    }
    public function setCreatedAt(DateTimeImmutable $createdAt) : self
    {
        $this->createdAt = $createdAt;
        return $this;
    }
    public function getBreed() : ?Breed
    {
        return $this->breed;
    }
    public function setBreed(Breed $breed) : self
    {
        $this->breed = $breed;
        return $this;
    }
    public function getImage() : ?Image
    {
        return $this->image;
    }
    public function setImage(?Image $image) : self
    {
        $this->image = $image;
        return $this;
    }
    /**
     * @return Collection<int, Aframe>
     */
    public function getAframes() : Collection
    {
        return $this->aframes;
    }
    public function addAframe(Aframe $aframe) : self
    {
        if (!$this->aframes->contains($aframe)) {
            $this->aframes->add($aframe);
            $aframe->setDogId($this);
        }
        return $this;
    }
    public function removeAframe(Aframe $aframe) : self
    {
        if ($this->aframes->removeElement($aframe)) {
            // set the owning side to null (unless already changed)
            if ($aframe->getDog() === $this) {
                $aframe->setDog(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Treadmill>
     */
    public function getTreadmills(): Collection
    {
        return $this->treadmills;
    }

    public function addTreadmill(Treadmill $treadmill): self
    {
        if (!$this->treadmills->contains($treadmill)) {
            $this->treadmills->add($treadmill);
            $treadmill->setDog($this);
        }

        return $this;
    }

    public function removeTreadmill(Treadmill $treadmill): self
    {
        if ($this->treadmills->removeElement($treadmill)) {
            // set the owning side to null (unless already changed)
            if ($treadmill->getDog() === $this) {
                $treadmill->setDog(null);
            }
        }

        return $this;
    }
}
