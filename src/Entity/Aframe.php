<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Repository\AframeRepository;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[ApiResource(
    operations: [
        new Get(
            normalizationContext: ['groups' => ['aframe:read'], 'swagger_definition_name' => 'Read']
        ),
        new GetCollection(
            normalizationContext: ['groups' => ['aframe:read']]
        ),
        new Put(
            denormalizationContext: ['groups' => ['aframe:update']],
            security: 'is_granted("ROLE_USER")'
        ),
        new Delete(security: 'is_granted("ROLE_USER")'),
        new Post(
            denormalizationContext: ['groups' => ['aframe:create'], 'swagger_definition_name' => 'Write'],
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Sorry, but only registered user can create the aframe record'
        )
    ]
)]
#[ORM\Entity(repositoryClass: AframeRepository::class)]
#[ORM\Table(name: 'aframe')]
#[ApiFilter(SearchFilter::class, properties: ['event' => 'exact', 'dog' => 'exact'])]
class Aframe
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[Groups(['aframe:create', 'aframe:read'])]
    private Uuid $id;

    #[ORM\ManyToOne(targetEntity: Dog::class, inversedBy: 'aframes')]
    #[Groups(['aframe:create', 'aframe:read', 'aframe:update'])]
    private Dog $dog;

    #[ORM\Column(type: 'float', length: 4, precision: 2, nullable: true, columnDefinition: 'FLOAT')]
    #[Groups(['aframe:create', 'aframe:read', 'aframe:update'])]
    private ?float $result1 = null;

    #[ORM\Column(type: 'float', length: 4, precision: 2, nullable: true, columnDefinition: 'FLOAT')]
    #[Groups(['aframe:create', 'aframe:read', 'aframe:update'])]
    private ?float $result2 = null;

    #[ORM\Column(type: 'float', length: 4, precision: 2, nullable: true, columnDefinition: 'FLOAT')]
    #[Groups(['aframe:create', 'aframe:read', 'aframe:update'])]
    private ?float $result3 = null;

    #[ORM\Column(type: 'float', length: 4, precision: 2, nullable: true, columnDefinition: 'FLOAT')]
    #[Groups(['aframe:create', 'aframe:read', 'aframe:update'])]
    private ?float $result4 = null;

    #[ORM\Column(type: 'float', length: 4, precision: 2, nullable: true, columnDefinition: 'FLOAT')]
    #[Groups(['aframe:create', 'aframe:read', 'aframe:update'])]
    private ?float $result5 = null;

    #[ORM\Column(type: 'float', length: 4, precision: 2, nullable: true, columnDefinition: 'FLOAT')]
    #[Groups(['aframe:create', 'aframe:read', 'aframe:update'])]
    private ?float $result6 = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['aframe:create', 'aframe:read', 'aframe:update'])]
    private ?bool $professional = null;

    #[ORM\Column(nullable: true)]
    private ?float $dog_height = null;

    #[ORM\Column]
    #[Groups(['aframe:create'])]
    private ?\DateTimeImmutable $created_at = null;

    #[ORM\ManyToOne( targetEntity: Event::class, inversedBy: 'aframes')]
    #[Groups(['aframe:create', 'aframe:read'])]
    private ?Event $event = null;


    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->setCreatedAt(
            new DateTimeImmutable('now', new DateTimeZone('Europe/Bratislava'))
        );
        $this->id = Uuid::v4();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(): self
    {
        $this->id = Uuid::v4();
        return $this;
    }

    public function getDog(): ?Dog
    {
        return $this->dog;
    }

    public function setDog(Dog $dog): self
    {
        $this->dog = $dog;
        return $this;
    }

    public function getResult1(): ?float
    {
        return $this->result1;
    }

    public function setResult1(?float $result1): self
    {
        $this->result1 = $result1;
        return $this;
    }

    public function getResult2(): ?float
    {
        return $this->result2;
    }

    public function setResult2(?float $result2): self
    {
        $this->result2 = $result2;
        return $this;
    }

    public function isProfessional(): ?bool
    {
        return $this->professional;
    }

    public function setProfessional(?bool $professional): self
    {
        $this->professional = $professional;
        return $this;
    }

    public function getDogHeight(): ?float
    {
        return $this->dog_height;
    }

    public function setDogHeight(?float $dog_height): self
    {
        $this->dog_height = $dog_height;
        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getResult3(): ?float
    {
        return $this->result3;
    }

    public function setResult3(?float $result3): static
    {
        $this->result3 = $result3;

        return $this;
    }

    public function getResult4(): ?float
    {
        return $this->result4;
    }

    public function setResult4(?float $result4): static
    {
        $this->result4 = $result4;

        return $this;
    }

    public function getResult5(): ?float
    {
        return $this->result5;
    }

    public function setResult5(?float $result5): static
    {
        $this->result5 = $result5;

        return $this;
    }

    public function getResult6(): ?float
    {
        return $this->result6;
    }

    public function setResult6(?float $result6): static
    {
        $this->result6 = $result6;

        return $this;
    }
}
