<?php declare (strict_types=1);

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\EventsRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [
        new Get(
            normalizationContext: ['groups' => ['event:create'], 'swagger_definition_name' => 'Write']),

        new Put(
            denormalizationContext: ['groups' => ['event:update'], 'swagger_definition_name' => 'Update'],
            security: 'object.owner == user || is_granted("ROLE_ADMIN")',
            securityMessage: 'Sorry, but you are not the event owner.'
        ),
        new Delete(security: 'object.owner == user || is_granted("ROLE_ADMIN")'),
        new GetCollection(),
        new Post(
            denormalizationContext: ['groups' => ['event:create'], 'swagger_definition_name' => 'Write'],
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Sorry, but only registered user can create the Event'
        )
    ],
    normalizationContext: [
        'groups' => ['event:read'],
        'swagger_definition_name' => 'Read',
        'datetime_format' => 'Y-m-d'
    ]
)]
#[ApiFilter(BooleanFilter::class, properties: ['workout', 'isPublished'])]
#[ApiFilter(DateFilter::class, properties: ['eventStart'])]
#[ApiFilter(OrderFilter::class, properties: ['eventStart', 'title', 'place'])]
#[ORM\Entity(repositoryClass: EventsRepository::class)]
#[ORM\Table(name: 'events')]
class Event
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[Groups(['event:create', 'event:read'])]
    private Uuid $id;

    #[Groups(['event:create', 'event:read', 'event:update'])]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'events')]
    #[ORM\JoinColumn(nullable: false)]
    public User $owner;

    #[Groups(['event:create', 'event:read', 'event:update'])]
    #[ORM\Column(type: 'datetime')]
    #[Assert\NotBlank]
    private ?DateTimeInterface $eventStart = null;

    #[Groups(['event:create', 'event:read', 'event:update'])]
    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?DateTimeInterface $eventEnd = null;

    #[Groups(['event:create', 'event:read', 'event:update'])]
    #[ORM\Column(type: 'string', length: 500)]
    private ?string $title = null;

    #[Groups(['event:create', 'event:read', 'event:update'])]
    #[ORM\Column(type: 'text')]
    private ?string $place = null;

    #[Groups(['event:create', 'event:read', 'event:update'])]
    #[ORM\Column(type: 'text')]
    private ?string $description = null;

    #[Groups(['event:create', 'event:read', 'event:update'])]
    #[ORM\Column(type: 'boolean')]
    private ?bool $isPublished = null;

    #[Groups(['event:create', 'event:read', 'event:update'])]
    #[ORM\OneToOne(targetEntity: Image::class, cascade: ['persist', 'remove'], orphanRemoval: true)]
    private ?Image $image = null;

    #[Groups(['event:create', 'event:read', 'event:update'])]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $seoDescription = null;

    #[Groups(['event:create', 'event:update'])]
    #[ORM\Column(type: 'boolean')]
    private ?bool $workout = null;

    #[ORM\OneToMany(mappedBy: 'event', targetEntity: Aframe::class)]
    private Collection $aframes;

    #[ORM\OneToMany(mappedBy: 'event', targetEntity: Treadmill::class, orphanRemoval: true)]
    private Collection $treadmills;

    public function __construct()
    {
        $this->aframes = new ArrayCollection();
        $this->id = Uuid::v6();
        $this->treadmills = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;
        return $this;
    }

    public function getEventStart(): ?DateTimeInterface
    {
        return $this->eventStart;
    }

    public function setEventStart(DateTimeInterface $start): self
    {
        $this->eventStart = $start;
        return $this;
    }

    public function getEventEnd(): ?DateTimeInterface
    {
        return $this->eventEnd;
    }

    public function setEventEnd(?DateTimeInterface $event_end): self
    {
        $this->eventEnd = $event_end;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(string $place): self
    {
        $this->place = $place;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;
        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        $this->image = $image;
        return $this;
    }

    public function getSeoDescription(): ?string
    {
        return $this->seoDescription;
    }

    public function setSeoDescription(?string $seoDescription): self
    {
        $this->seoDescription = $seoDescription;
        return $this;
    }

    public function isWorkout(): ?bool
    {
        return $this->workout;
    }

    public function setWorkout(bool $workout): self
    {
        $this->workout = $workout;
        return $this;
    }

    /**
     * @return Collection<int, Aframe>
     */
    public function getAframes(): Collection
    {
        return $this->aframes;
    }

    public function addAframe(Aframe $aframe): self
    {
        if (!$this->aframes->contains($aframe)) {
            $this->aframes->add($aframe);
            $aframe->setEvent($this);
        }

        return $this;
    }

    public function removeAframe(Aframe $aframe): self
    {
        if ($this->aframes->removeElement($aframe)) {
            // set the owning side to null (unless already changed)
            if ($aframe->getEvent() === $this) {
                $aframe->setEvent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Treadmill>
     */
    public function getTreadmills(): Collection
    {
        return $this->treadmills;
    }

    public function addTreadmill(Treadmill $treadmill): self
    {
        if (!$this->treadmills->contains($treadmill)) {
            $this->treadmills->add($treadmill);
            $treadmill->setEvent($this);
        }

        return $this;
    }

    public function removeTreadmill(Treadmill $treadmill): self
    {
        if ($this->treadmills->removeElement($treadmill)) {
            // set the owning side to null (unless already changed)
            if ($treadmill->getEvent() === $this) {
                $treadmill->setEvent(null);
            }
        }

        return $this;
    }
}
