<?php declare (strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\CompetitionRepository;
use Symfony\Component\Serializer\Annotation\Context;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    description: 'Typ sutaze v bull sportoch',
    operations: [
        new Get(security: 'is_granted("ROLE_USER")'),
        new Put(security: 'is_granted("ROLE_ADMIN")'),
        new GetCollection(security: 'is_granted("ROLE_USER")'),
        new Post(security: 'is_granted("ROLE_ADMIN")')
    ],
    normalizationContext: [
        'groups' => ['competition:read']
    ],
    denormalizationContext: [
        'groups' => ['competition:write']]
)]
#[ORM\Entity(repositoryClass: CompetitionRepository::class)]
class Competition
{
    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->created_at = new \DateTimeImmutable('now', new \DateTimeZone('Europe/Bratislava'));
    }

    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[Groups(['competition:read', 'competition:write'])]
    private $id;

    #[Groups(['competition:read', 'competition:write', 'event:read'])]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $name = null;

    #[Groups(['competition:read', 'competition:write', 'event:read'])]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $nameSk = null;

    #[Groups(['competition:read', 'competition:write'])]
    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $description = null;

    #[Groups(['competition:read', 'competition:write'])]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'competitions')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotBlank]
    private ?User $author = null;

    #[Groups(['competition:read'])]
    #[Context(normalizationContext: [DateTimeNormalizer::FORMAT_KEY => 'Y-m-d H:i:s'])]
    #[ORM\Column(type: 'datetime_immutable')]
    private ?\DateTimeImmutable $created_at;

    #[Groups(['competition:read', 'competition:write'])]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $seoDescription = null;

    #[Groups(['competition:read', 'competition:write'])]
    #[ORM\OneToOne(targetEntity: Image::class, cascade: ['persist', 'remove', 'detach'], orphanRemoval: true)]
    private ?Image $image = null;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id) : self
    {
        $this->id = $id;
        return $this;
    }

    public function getName() : ?string
    {
        return $this->name;
    }

    public function setName(string $name) : self
    {
        $this->name = $name;
        return $this;
    }

    public function getNameSk() : ?string
    {
        return $this->nameSk;
    }

    public function setNameSk(string $name_sk) : self
    {
        $this->nameSk = $name_sk;
        return $this;
    }

    public function getDescription() : ?string
    {
        return $this->description;
    }

    public function setDescription(string $description) : self
    {
        $this->description = $description;
        return $this;
    }

    public function getAuthor() : ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author) : self
    {
        $this->author = $author;
        return $this;
    }

    public function getCreatedAt() : ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at) : self
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function getImage() : ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image) : self
    {
        $this->image = $image;
        return $this;
    }

    public function getSeoDescription() : ?string
    {
        return $this->seoDescription;
    }

    public function setSeoDescription(?string $seoDescription) : self
    {
        $this->seoDescription = $seoDescription;
        return $this;
    }
}
