<?php declare (strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\ImagesRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[ApiResource(
    description: 'Zoznam obrazkov',
    operations: [
        new Get(),
        new Delete(),
        new Put(),
        new GetCollection(),
        new Post()
    ],
    normalizationContext: [
        'groups' => ['image:read']
    ],
    denormalizationContext: [
        'groups' => ['image:write']
    ]
)]
#[ORM\Entity(repositoryClass: ImagesRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'images')]
class Image
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[Groups(['image:read', 'image:write'])]
    private Uuid $id;

    #[Groups(['image:read', 'image:write'])]
    #[ORM\Column(type: 'text')]
    private string $AWSname;

    #[Groups(['image:read', 'image:write'])]
    #[ORM\Column(type: 'text')]
    private string $name;

    #[Groups(['image:read', 'image:write'])]
    #[ORM\Column(type: 'text')]
    private string $mimeType;

    #[ORM\Column(type: 'datetime')]
    private DateTime $createdAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?DateTimeInterface $updatedAt = null;

    #[ORM\PrePersist]
    public function onPrePersist(): void
    {
        $this->setCreatedAt(new \DateTime('now', new \DateTimeZone('Europe/Bratislava')));
    }

    #[ORM\PreUpdate]
    public function onPreUpdate(): void
    {
        $this->setUpdatedAt(new \DateTime('now', new \DateTimeZone('Europe/Bratislava')));
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId($id) : self
    {
        $this->id = $id;
        return $this;
    }

    public function getAWSname() : ?string
    {
        return $this->AWSname;
    }

    public function setAWSname(string $AWSname): self
    {
        $this->AWSname = $AWSname;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    public function setMimeType(string $mimeType): self
    {
        $this->mimeType = $mimeType;
        return $this;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
