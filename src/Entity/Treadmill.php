<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Repository\TreadmillRepository;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;


#[ApiResource(
    operations: [
        new Get(
            normalizationContext: ['groups' => ['treadmill:read'], 'swagger_definition_name' => 'Read']
        ),
        new GetCollection(
            normalizationContext: ['groups' => ['treadmill:read']]
        ),
        new Put(
            denormalizationContext: ['groups' => ['treadmill:update']],
            security: 'is_granted("ROLE_USER")'
        ),
        new Delete(security: 'is_granted("ROLE_USER")'),
        new Post(
            denormalizationContext: ['groups' => ['treadmill:create'], 'swagger_definition_name' => 'Write'],
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Sorry, but only registered user can create the treadmill record'
        )
    ]
)]
#[ApiFilter(SearchFilter::class, properties: ['event' => 'exact'])]
#[ORM\Entity(repositoryClass: TreadmillRepository::class)]
class Treadmill
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[Groups(['treadmill:create', 'treadmill:read'])]
    private Uuid $id;

    #[ORM\Column(nullable: true)]
    #[Groups(['treadmill:create', 'treadmill:read', 'treadmill:update'])]
    private ?float $result1 = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['treadmill:create', 'treadmill:read', 'treadmill:update'])]
    private ?float $result2 = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['treadmill:create', 'treadmill:read', 'treadmill:update'])]
    private ?float $result3 = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['treadmill:create', 'treadmill:read', 'treadmill:update'])]
    private ?float $result4 = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['treadmill:create', 'treadmill:read', 'treadmill:update'])]
    private ?float $avgSpeed = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['treadmill:create', 'treadmill:read', 'treadmill:update'])]
    private ?float $maxSpeed = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['treadmill:create', 'treadmill:read'])]
    private ?bool $professional = null;

    #[ORM\ManyToOne(inversedBy: 'treadmills')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['treadmill:create', 'treadmill:read'])]
    private ?Dog $dog = null;

    #[ORM\ManyToOne(inversedBy: 'treadmills')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['treadmill:create', 'treadmill:read'])]
    private ?Event $event = null;

    #[ORM\Column]
    #[Groups(['treadmill:create'])]
    private ?\DateTimeImmutable $created_at = null;

    public function __construct()
    {
        $this->created_at = new DateTimeImmutable('now', new DateTimeZone('Europe/Bratislava'));
        $this->id = Uuid::v4();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(): self
    {
        $this->id = Uuid::v4();
        return $this;
    }

    public function getResult1(): ?float
    {
        return $this->result1;
    }

    public function setResult1(?float $result1): self
    {
        $this->result1 = $result1;

        return $this;
    }

    public function getResult2(): ?float
    {
        return $this->result2;
    }

    public function setResult2(?float $result2): self
    {
        $this->result2 = $result2;

        return $this;
    }

    public function getResult3(): ?float
    {
        return $this->result3;
    }

    public function setResult3(?float $result3): self
    {
        $this->result3 = $result3;

        return $this;
    }

    public function getResult4(): ?float
    {
        return $this->result4;
    }

    public function setResult4(float $result4): self
    {
        $this->result4 = $result4;

        return $this;
    }

    public function getAvgSpeed(): ?float
    {
        return $this->avgSpeed;
    }

    public function setAvgSpeed(?float $avgSpeed): self
    {
        $this->avgSpeed = $avgSpeed;

        return $this;
    }

    public function getMaxSpeed(): ?float
    {
        return $this->maxSpeed;
    }

    public function setMaxSpeed(?float $maxSpeed): self
    {
        $this->maxSpeed = $maxSpeed;

        return $this;
    }

    public function isProfessional(): ?bool
    {
        return $this->professional;
    }

    public function setProfessional(?bool $professional): self
    {
        $this->professional = $professional;

        return $this;
    }

    public function getDog(): ?Dog
    {
        return $this->dog;
    }

    public function setDog(?Dog $dog): self
    {
        $this->dog = $dog;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }
}
