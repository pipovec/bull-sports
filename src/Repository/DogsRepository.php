<?php

namespace App\Repository;

use App\Entity\Dog;
use App\Entity\User;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Dog|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dog|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dog[]    findAll()
 * @method Dog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DogsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Dog::class);
    }

    /**
     * Save dog to the database
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createDog(User $user, string $name, string $breed, float $weight, string $born)
    {
        $dog = new Dog();

        $dog->setName($name);
        $dog->setBreed($breed);
        $dog->setWeight($weight);
        $dog->setBirthdate(new DateTimeImmutable($born));
        $dog->setOwner($user);

        $this->getEntityManager()->persist($dog);
        $this->getEntityManager()->flush();
    }

    public function getAllMyDogs(User $user): array
    {
        return $this->findBy([
           'owner' => $user->getId(),
        ]);
    }
}
