const Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}
const isProduction = Encore.isProduction();


Encore
    .configureDefinePlugin(options => {
        options.__VUE_PROD_DEVTOOLS__ = JSON.stringify(isProduction)
    })
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')

    .addEntry('app', './assets/app.js')
    .addStyleEntry('style', './assets/styles/tailwind.css')
    .addEntry('base', './assets/styles/base.sass')
    .addEntry('profile', './assets/styles/profile.sass')
    .addStyleEntry('ck-editor-content', './assets/styles/ck-editor-content.sass')

    .addEntry('vue-profile', './assets/vue/Profile/main.js')
    .addEntry('vue-admin', './assets/vue/Admin/main.js')
    .addEntry('vue-cms', './assets/vue/CMS/main.js')

    .enableVueLoader()
    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })

    // enables @babel/preset-env polyfills
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })

    .enableSassLoader()
    .enablePostCssLoader((options) => {
        // new option outlined here https://webpack.js.org/loaders/postcss-loader/
        options.postcssOptions = {
            config: './postcss.config.js',
        }
    })
;

module.exports = Encore.getWebpackConfig();
