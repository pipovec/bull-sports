import {createStore} from "vuex";
import axios from "axios";

export default createStore({
    state: {
        user: {},
        iriUser: null,
        breeds: {},
        apiToken: null,
        myDogs: {},
        availableEvents: {},
        myWorkouts: {},
        aws: {},
        listOfTreadmillsByEvent: {},
        listOfAframeByEvent: {},
    },
    mutations: {
        SET_USER(state, data) {
            state.user = data;
        },
        SET_IRI_USER(state, data) {
            state.iriUser = data;
        },
        SET_API_TOKEN(state, data) {
            state.apiToken = data;
        },
        SET_MY_DOGS(state, dogs) {
            state.myDogs = dogs;
        },
        SET_BREEDS(state, data) {
            state.breeds = data;
        },
        SET_AVAILABLE_EVENTS(state, data) {
            state.availableEvents = data
        },
        SET_MY_WORKOUTS(state, data) {
            state.myWorkouts = data;
        },
        SET_AWS(state, data) {
            state.aws = data;
        },
        SET_TREADMILLS_BY_EVENT(state, data) {
            state.listOfTreadmillsByEvent = data;
        },
        SET_AFRAMES_BY_EVENT(state, data) {
            state.listOfAframeByEvent = data;
        }
    },
    getters: {
        apiToken({apiToken}) {
            return apiToken;
        },
        iriUser({iriUSer}) {
            return iriUSer;
        },
        myDogs({myDogs}) {
            return myDogs;
        },
        user({user}) {
            return user;
        },
        getBreeds: (state) => {
            return state.breeds;
        },
    },
    actions: {
        async GET_MY_DOGS({commit, state}) {
            await axios.get('/api/dogs', {
                headers: {
                    'accept': 'application/ld+json',
                    'X-AUTH-TOKEN': state.apiToken,
                },
                params: {'owner': state.iriUser}
            }).then(response => {
                commit('SET_MY_DOGS', response.data['hydra:member']);
            })
        },

        async GET_USER({commit, state}) {
            await axios.get(state.iriUser, {
                headers: {
                    'accept': 'application/ld+json',
                    'X-AUTH-TOKEN': state.apiToken,
                }
            }).then(response => {
                commit('SET_USER', response.data);
            })
        },

        async GET_BREEDS({commit, state}) {
            await axios.get('/api/breeds', {
                headers: {
                    'accept': 'application/ld+json',
                    'X-AUTH-TOKEN': state.apiToken,
                }
            }).then(response => {
                commit('SET_BREEDS', response.data['hydra:member'])
            })
        },
        async GET_AVAILABLE_EVENTS({commit, state}) {
            await axios.get('/api/events', {
                headers: {
                    'accept': 'application/ld+json',
                    'X-AUTH-TOKEN': state.apiToken,
                },
                params: {
                    'isPublished': true,
                    'eventStart[after]': '2021-11-07'
                }
            }).then(response => {
                commit('SET_AVAILABLE_EVENTS', response.data['hydra:member'])
            })
        },
        async GET_MY_WORKOUTS({commit, state}) {
            await axios.get('/api/events', {
                headers: {
                    'accept': 'application/ld+json',
                    'X-AUTH-TOKEN': state.apiToken,
                },
                params: {
                    'owner_id': state.iriUser,
                    'workout': true,
                }
            }).then(response => {
                commit('SET_MY_WORKOUTS', response.data['hydra:member'])
            })
        },
        async GET_TREADMILLS_BY_EVENT({commit, state}, event) {
            const params = new URLSearchParams([['event', event]])
            await axios.get('/api/treadmills', {params})
                .then(response => {
                    commit('SET_TREADMILLS_BY_EVENT', response.data['hydra:member'])
                })
        },
        async GET_AFRAMES_BY_EVENT({commit, state}, event) {
            const params = new URLSearchParams([['event', event]])
            await axios.get('/api/aframes', {params})
                .then(response => {
                    commit('SET_AFRAMES_BY_EVENT', response.data['hydra:member'])
                })
        }
    }
});
