import { createApp }from 'vue';
import Profile from "./Profile";
import router from "./router";
import store from "./store";

const dataFromApp = document.querySelector("#profile");

createApp(Profile, {...dataFromApp.dataset})
    .use(router)
    .use(store)
    .mount('#profile');
