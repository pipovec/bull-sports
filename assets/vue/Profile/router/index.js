import {createRouter, createWebHistory} from "vue-router";

import myDogs from "../views/myDogs";
import profileView from "../views/profileView";
import myEvents from "../views/myEvents";
import myWorkout from "../views/myWorkout";

const routes = [
    {
        path: '/profile/',
        name: 'profile',
        component: profileView,
        props: true,
        meta: {
            'title': 'prehľad',
        },
    },
    {
        path: '/profile/moji-psy/',
        name: 'my-dogs',
        component: myDogs,
        meta: {
            'title': 'editacia psov',
        },
    },
    {
        path: '/profile/moje-akcie/',
        name: 'moje-akcie',
        component: myEvents,
        meta: {
            'title': 'sutaze',
        },
    },
    {
        path: '/profile/trening/',
        name: 'trening',
        component: myWorkout,
        meta: {
            'title': 'Trening',
        },
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router
