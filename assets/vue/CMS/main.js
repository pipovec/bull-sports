import {createApp} from 'vue';
import CMS from "./CMS";
import router from "./router";

const dataFromApp = document.querySelector("#cms");

createApp(CMS, {...dataFromApp.dataset})
    .use(router)
    .mount('#cms');
