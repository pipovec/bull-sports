import {createRouter, createWebHistory} from "vue-router";
import articles from "../views/articles.vue";
import addArticle from "../views/addArticle.vue";

const routes = [
    {
        path: '/cms/clanky/',
        name: 'articles',
        component: articles,
        props: false,
        meta: {
            title: 'Články',
        },
    },
    {
        path: '/cms/clanky/pridat',
        name: 'articles-add',
        component: addArticle,
        props: false,
        meta: {
            title: 'Pridávanie článkov',
        },
    },

]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router
