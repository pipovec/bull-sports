import { createApp }from 'vue';
import Admin from "./Admin";
import router from "./router";
import store from "./store";

const dataFromApp = document.querySelector("#admin");

createApp(Admin, {...dataFromApp.dataset})
    .use(router)
    .use(store)
    .mount('#admin');