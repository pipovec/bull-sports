import {createRouter, createWebHistory} from "vue-router";
import homeView from "../views/homeView";
import usersView from "../views/usersView";
import eventsView from "../views/eventsView";
import breedView from "../views/breedView";
import competitionView from "../views/competitionView";
import competitionEditView from "../views/competitionEditView";

const routes = [
    {
        path: '/admin/domov/',
        name: 'domov',
        component: homeView,
        props: true,
        meta: {
            title: 'prehľad',
        },
    },
    {
        path: '/admin/uzivatelia/',
        name: 'uzivatelia',
        component: usersView,
        props: true,
        meta: {
          title: 'správa užívateľov',
        },
    },
    {
        path: '/admin/akcie/',
        name: 'akcie',
        component: eventsView,
        props: true,
        meta: {
            title : 'športové udalosti - akcie',
        }
    },
    {
        path: '/admin/discipliny/',
        name: 'disciplíny',
        component: competitionView,
        props: true,
        meta: {
            title : 'športové disciplíny',
        }
    },
    {
        path: '/admin/discipliny/:competitionId',
        name: 'úprava-disciplíny',
        component: competitionEditView,
        props: true,
        meta: {
            title : 'športové disciplíny',
        }
    },
    {
        path: '/admin/discipliny/:competitionId/:slug',
        name: 'úprava-disciplíny-slug',
        component: competitionEditView,
        props: true,
        meta: {
            title : 'športové disciplíny',
        }
    },
    {
        path: '/admin/plemena',
        name: 'plemena',
        component: breedView,
        props: true,
        meta: {
            title: 'zoznam plemien',
        }
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router
