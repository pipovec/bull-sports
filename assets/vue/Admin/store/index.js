import {createStore} from "vuex";
import axios from "axios";

export default createStore({
  state: {
    users: {},
    events: {},
    competitions: {},
    apiToken: '',
    iriUser: '',
    breeds: {},
    aws: {},
  },
  getters: {
    GET_USERS({users}) {
      return users;
    },
    getCompetition: (state) => (id) =>  {
      return state.competitions['hydra:member'].find(
        competition => competition.id === id
      )
    }
  },
  mutations: {
    SET_USERS(state, data) {
      state.users = data;
    },
    SET_API_TOKEN(state, data) {
      state.apiToken = data;
    },
    SET_IRI_USER(state, data) {
      state.iriUser = data;
    },
    SET_BREEDS(state, data) {
      state.breeds = data;
    },
    SET_COMPETITION_LIST(state, data) {
      state.competitions = data;
    },
    SET_EVENT_LIST(state, data) {
      state.events = data;
    },
    SET_AWS(state, data) {
      state.aws = data;
    }
  },
  actions: {
    async GET_USERS({commit, state}) {
      await axios.get('/api/users', {
        headers: {
          'accept': 'application/json',
          'X-AUTH-TOKEN': state.apiToken,
        }
      }).then(response => {
        commit('SET_USERS', response.data);
      })
    },
    async GET_BREEDS({commit, state}) {
      await axios.get('/api/breeds', {
        headers: {
          'accept': 'application/json',
          'X-AUTH-TOKEN': state.apiToken,
        }
      }).then(response => {
        commit('SET_BREEDS', response.data)
      })
    },
    async GET_COMPETITION_LIST({commit, state}) {
      await axios
        .get('/api/competitions', {
          headers: {
            'accept': 'application/ld+json',
            'X-AUTH-TOKEN': state.apiToken,
          }
        })
        .then(response => {
          commit('SET_COMPETITION_LIST', response.data)
        })
    },
    async SAVE_COMPETITION({state}, competitionPayload) {
      axios.put('/api/competitions/' + competitionPayload.id , competitionPayload, {
        headers: {
          'accept': 'application/ld+json',
          'X-AUTH-TOKEN': state.apiToken,
        }
      })
        .then(response => {

        });
    },
    async GET_EVENT_LIST({commit, state}) {
      await axios
        .get('/api/events', {
          headers: {
            'accept': 'application/ld+json',
            'X-AUTH-TOKEN': state.apiToken,
          }
        })
        .then(response => {
          commit('SET_EVENT_LIST', response.data)
        })
    },
  },
});
