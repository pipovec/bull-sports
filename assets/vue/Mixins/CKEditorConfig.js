export default {
  toolbar: [
    'heading', '|',
    'bold', 'italic','numberedList', 'bulletedList',
    'link','|', 'undo', 'redo'
  ],
  heading: {
    options: [
      { model: 'paragraph', title: 'Odsek', class: 'block ' },
      { model: 'heading1', view: 'h2', title: 'Nadpis 1', class: 'inline font-bold' },
      { model: 'heading2', view: 'h3', title: 'Nadpis 2', class: 'inline font-semibold'}
    ]
  },
  numberedList: {
    options: {
      model: 'numberedList', title: '', class: 'list-decimal'
    }
  },
  style: {
    width: '100%'
  }
}
