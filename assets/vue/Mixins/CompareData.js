export default function(propData, editedData) {

    if (JSON.stringify(propData) === JSON.stringify(editedData)) {
      return false;
    }

    let changes = {};

    Object.keys(editedData).forEach(key => {
      if (editedData[key] !== propData[key]) {
          changes[key] = editedData[key]
      }
    })

    return changes;
}