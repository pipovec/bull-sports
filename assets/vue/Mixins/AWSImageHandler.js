import { PutObjectCommand, DeleteObjectCommand, S3, S3Client } from "@aws-sdk/client-s3";

export async function handleFileUpload(file, fileName, awsParams) {

  const credentials = {
    "accessKeyId": awsParams.credentials.accessKeyId,
    "secretAccessKey": awsParams.credentials.secretKeyId
  }

  const s3Client = new S3({
    region: 'eu-central-1',
    credentials: credentials,
  });


  const bucketParams = {
    Bucket: awsParams.bucketName,
    Key: fileName,
    Body: file,
    ACL: 'public-read',
    headers: {
      'Content-Type': file.type,
      'Cache-Control': 'public,max-age=30240000',
    },
    Metadata: {
      'Content-Type': file.type,
      'Cache-Control': 'public,max-age=30240000',
    },
  };

  let result = undefined

  try {
    await s3Client
        .send(new PutObjectCommand(bucketParams))
        .then((response) => {
          result = response;
        }).catch(e => {
          result = e.message;
        })
  } catch (err) {
    result = err.message;
  }

  return result
}

export async function handleFileDelete(fileName, awsParams) {

  let result = undefined;


  const credentials = {
    "accessKeyId": awsParams.credentials.accessKeyId,
    "secretAccessKey": awsParams.credentials.secretKeyId
  }

  const s3client = new S3Client({
    region: 'eu-central-1',
    credentials: credentials,
  });

  const deleteParams = {
    Bucket: awsParams.bucketName,
    Key: fileName,
  };

  try {
    await s3client.send(new DeleteObjectCommand(deleteParams))
      .then((data) => {
       console.log("Data has been deleted", data)
      })
  } catch (err) {
    result = err.message;
  }

  return result;
}
