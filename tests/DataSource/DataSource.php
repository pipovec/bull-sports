<?php declare(strict_types=1);


namespace App\Tests\DataSource;


/**
 * @author Boris Fekar
 * @createdAt 3. 10. 2021
 * @package  App\Test\DataSource
 */
final class DataSource
{
    use EventDataSource;
    use CompetitionDataSource;
    use ImageDataSource;
}