<?php declare(strict_types=1);


namespace App\Tests\DataSource;


use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\Uid\Uuid;

/**
 * @author Boris Fekar
 * @createdAt 3. 10. 2021
 * @package  App\Test\DataSource
 */
trait CompetitionDataSource
{
    /**
     * @return string[]
     */
    #[ArrayShape(["id" => "\Symfony\Component\Uid\UuidV4", 'name' => "string", 'nameSk' => "string", 'description' => "string"])]
    public function competition(): array
    {
        return [
            "id" => Uuid::v4(),
            'name' => 'Test competition 1',
            'nameSk' => 'Testovacia disciplina 1',
            'description' => 'Testing competition 1',
            'seoDescription' => 'Test seo desctiption 1',
        ];
    }

    /**
     * @return string[]
     */
    #[ArrayShape(["id" => "\Symfony\Component\Uid\UuidV4", 'name' => "string", 'nameSk' => "string", 'description' => "string"])]
    public function competition2(): array
    {
        return [
            "id" => Uuid::v4(),
            'name' => 'Test competition 2',
            'nameSk' => 'Testovacia disciplina 2',
            'description' => 'Testing competition 2',
            'seoDescription' => "Seo popis 2",
        ];
    }
}