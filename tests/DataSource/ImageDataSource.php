<?php declare(strict_types=1);


namespace App\Tests\DataSource;


use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Boris Fekar
 * @createdAt 3. 10. 2021
 * @package  App\Test\DataSource
 */
trait ImageDataSource
{
    /**
     * Image data source without id
     * @return string[]
     */
    #[ArrayShape(['awsname' => "string", 'name' => "string", 'mimeType' => "string"])]
    public function imageSource(): array
    {
        return [
            'AWSname' => 'events/6aff2d3b-de02-4c41-8c9c-fad7658ac57d.lara_a_frame.jpg',
            'name' => 'lara_a_frame.jpg',
            'mimeType' => 'image/webp'
        ];
    }
}