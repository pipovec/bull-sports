<?php declare(strict_types=1);


namespace App\Tests\DataSource;


use Symfony\Component\Uid\Uuid;

/**
 * @author Boris Fekar
 * @createdAt 3. 10. 2021
 * @package  App\Test\DataSource
 */
trait EventDataSource
{
    /**
     * Event without discipline
     *
     * @return array
     */
    public function event(): array
    {
        return [
            "id" => Uuid::v6(),
            "owner" => "",
            "eventStart" => "2021-09-13T15:55:45.127Z",
            "title" => "Test A",
            "place" => "Bratislava - Trnavka",
            "description" => "Some description about event",
            "eventCompetitions" => [],
            "isPublished" => true,
            "eventEnd" => "2021-09-13T15:55:45.127Z"
        ];
    }

    /**
     * Event with one discipline
     *
     * @return array
     */
    public function eventWithOneDiscipline(): array
    {
        $event = [
            "id" => Uuid::v4(),
            "eventStart" => "2021-09-13T15:55:45.127Z",
            "eventEnd" => "2021-09-13T15:55:45.127Z",
            "title" => "Udalost s jednou disciplinou",
            "place" => "Bratislava - Trnavka",
            "description" => "Some description about event",
            "isPublished" => false,
        ];

        $discipline = [
            "typeOfCompetition" => "/api/competitions/321321",
            "start" => "2021-09-13T15:55:45.127Z",
            "ends" => "2021-09-13T17:55:45.127Z",
            "description" => "Prva disciplina"
        ];

        $event['eventCompetitions'][] = $discipline;

        return $event;
    }

    /**
     * Event with one discipline
     *
     * @return array
     */
    public function eventWithTwoDisciplines(): array
    {
        $event = [
            "id" => Uuid::v4(),
            "eventStart" => "2021-09-13T15:55:45.127Z",
            "eventEnd" => "2021-09-13T15:55:45.127Z",
            "title" => "Udalost s jednou disciplinou",
            "place" => "Bratislava - Trnavka",
            "description" => "Some description about event",
            "isPublished" => false,
        ];

        $discipline = [
            "typeOfCompetition" => "",
            "start" => "2021-09-13T15:55:45.127Z",
            "ends" => "2021-09-13T17:55:45.127Z",
            "description" => "First competition"
        ];

        $discipline2 = [
            "typeOfCompetition" => "",
            "start" => "2021-09-13T15:55:45.127Z",
            "ends" => "2021-09-13T17:55:45.127Z",
            "description" => "Second competition"
        ];

        $event['eventCompetitions'][] = $discipline;
        $event['eventCompetitions'][] = $discipline2;

        return $event;
    }
}