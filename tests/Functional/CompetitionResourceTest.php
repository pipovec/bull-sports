<?php declare(strict_types=1);


namespace App\Tests\Functional;

use App\Test\CustomApiTestCase;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * @author Boris Fekar
 * @createdAt 4. 9. 2021
 * @package  App\Tests\Functional
 */
class CompetitionResourceTest extends CustomApiTestCase
{
    use ReloadDatabaseTrait;

    const API_ENDPOINT = '/api/competitions';

    public function testCreateCompetitionUnauthorized()
    {
        $client = self::createClient();

        $client->request('POST', self::API_ENDPOINT, [
            'headers' => [
                'Content-type' => 'application/json'
            ],
            'json' => $this->dataSource->competition(),
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function testCreateCompetition()
    {
        $client = self::createClient();

        $userIRI = $this->createUser(
            'boris@admin.com',
            'Boris',
            'Admin',
            self::ADMIN_PASSWORD,
            array('ROLE_USER','ROLE_ADMIN'),
            self::ADMIN_API_TOKEN
        );

        $newCompetition = $this->competitionData();
        $newCompetition['author'] = $userIRI;

        // Create competition
        $client->request("POST", self::API_ENDPOINT, [
            'headers' => [
                'X-AUTH-TOKEN' => self::ADMIN_API_TOKEN,
                'Content-type' => 'application/ld+json',
            ],
            'json' => $newCompetition,

        ]);
        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);


        $nextCompetition = [
            "author"=> $userIRI,
            "id" => "d96c8aa1-b70b-43bf-82ba-c49f35a848dd",
            "name"=>"Test ",
            "nameSk" => "Teststst tst",
            "description"=>"Tests test etsts"
        ];

        $result = $client->request("POST", self::API_ENDPOINT, [
            'headers' => [
                'X-AUTH-TOKEN' => self::ADMIN_API_TOKEN,
                'Content-type' => 'application/ld+json',
            ],
            'json' => $nextCompetition

        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
    }

    /**
     * @return string[]
     */
    private function competitionData(): array
    {
        return [
            'id' => 'd4cdf9a2-80f9-46c0-ab50-15b0427dd9f8',
            'name' => 'Street jump Lara',
            'nameSk' => 'Poulicne skakanie',
            'description' => 'Skaceme po ulici cez pneumatiky, lebo sme vychovany ulicou. Jeee bracho, chapes?',
        ];
    }
}