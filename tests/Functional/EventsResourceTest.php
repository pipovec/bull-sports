<?php declare(strict_types=1);


namespace App\Tests\Functional;

use App\Test\CustomApiTestCase;
use App\Tests\DataSource\DataSource;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use Symfony\Component\HttpFoundation\Response;


/**
 * @author Boris Fekar
 * @createdAt 13. 9. 2021
 * @package  App\Tests\Functional
 */
class EventsResourceTest extends CustomApiTestCase
{
    use ReloadDatabaseTrait;

    const API_ENDPOINT = '/api/events';

    public function testCreateEventsWithOneCompetitions()
    {

        $client = self::createClient();
        $this->dataSource = new DataSource();
        $userIRI = $this->createUserIri();

        ### CREATE TEST COMPETITION AND GET THEY IRI###
        $newCompetition = $this->dataSource->competition();
        $newCompetition['author'] = $userIRI;

        $newCompetition2 = $this->dataSource->competition();
        $newCompetition2['author'] = $userIRI;

        ### FIRS COMPETITION ###
        $result = $client->request("POST", '/api/competitions', [
            'headers' => [
                'Content-type' => 'application/ld+json',
                'X-AUTH-TOKEN' => self::ADMIN_API_TOKEN,
            ],
            'json' => $newCompetition,
        ]);
        $this->assertResponseStatusCodeSame(201, "Disciplina 1 bola vytvorena");
        $result = json_decode($result->getContent(), true);
        $competitionIRI = $result['@id'];

        ### SECOND COMPETITION
        $result = $client->request("POST", '/api/competitions', [
            'headers' => [
                'Content-type' => 'application/ld+json',
                'X-AUTH-TOKEN' => self::ADMIN_API_TOKEN,
            ],
            'json' => $newCompetition2,
        ]);
        $this->assertResponseStatusCodeSame(201, "Disciplina 2 bola vytvorena");
        $result = json_decode($result->getContent(), true);
        $competitionIRI2 = $result['@id'];


        // Creating an event with one discipline
        $newEvent = $this->dataSource->eventWithOneDiscipline();
        $newEvent['owner'] = $userIRI;
        $newEvent['eventCompetitions'][0]['typeOfCompetition'] = $competitionIRI;

        $result = $client->request("POST", self::API_ENDPOINT, [
            'headers' => [
                'Content-type' => 'application/ld+json',
                'X-AUTH-TOKEN' => self::ADMIN_API_TOKEN,
            ],
            'json' => $newEvent,
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
    }

    public function testCreateEventsWithTwoCompetitions()
    {
        $client = self::createClient();
        $userIRI = $this->createUserIri();

        ### CREATE TEST COMPETITION AND GET THEY IRI###
        $newCompetition = $this->dataSource->competition();
        $newCompetition['author'] = $userIRI;

        $newCompetition2 = $this->dataSource->competition2();
        $newCompetition2['author'] = $userIRI;

        ### FIRS COMPETITION ###
        $result = $client->request("POST", '/api/competitions', [
            'headers' => [
                'Content-type' => 'application/ld+json',
                'X-AUTH-TOKEN' => self::ADMIN_API_TOKEN,
            ],
            'json' => $newCompetition,
        ]);
        $this->assertResponseStatusCodeSame(201, "Disciplina 1 bola vytvorena");

        $result = json_decode($result->getContent(), true);
        $competitionIRI = $result['@id'];

        ### SECOND COMPETITION
        $result = $client->request("POST", '/api/competitions', [
            'headers' => [
                'Content-type' => 'application/ld+json'
            ],
            'json' => $newCompetition2,
        ]);
        $this->assertResponseStatusCodeSame(201, "Disciplina 2 bola vytvorena");
        $result = json_decode($result->getContent(), true);
        $competitionIRI2 = $result['@id'];


        // Creating an event with two discipline
        $newEvent = $this->dataSource->eventWithTwoDisciplines();
        $newEvent['owner'] = $userIRI;
        $newEvent['eventCompetitions'][0]['typeOfCompetition'] = $competitionIRI;
        $newEvent['eventCompetitions'][1]['typeOfCompetition'] = $competitionIRI2;


        $client->request("POST", self::API_ENDPOINT, [
            'headers' => [
                'Content-type' => 'application/ld+json',
                'X-AUTH-TOKEN' => self::ADMIN_API_TOKEN,
            ],
            'json' => $newEvent,
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED, "The event was created with two competitions");
    }

    public function testCreateEventsWithoutCompetitions()
    {
        $client = self::createClient();
        $userIRI = $this->createUserIri();

        $newEvent = $this->dataSource->event();
        $newEvent['owner'] = $userIRI;


        $client->request("POST", self::API_ENDPOINT, [
            'headers' => [
                'Content-type' => 'application/ld+json',
                'X-AUTH-TOKEN' => self::ADMIN_API_TOKEN,
            ],
            'json' => $newEvent,
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
        $this->assertJsonContains(['title' => 'Test A']);

        $client->request("GET", self::API_ENDPOINT, [
            'headers' => [
                'Content-type' => 'application/ld+json'
            ],
        ]);
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testCreateEventWithImage()
    {
        $client = self::createClient();
        $userIRI = $this->createUserIri();

        $newEvent = $this->dataSource->event();
        $newEvent['owner'] = $userIRI;
        $newEvent['image'] = $this->dataSource->imageSource();
        $newEvent['image']['id'] = $newEvent['id'];

        $client->request("POST", self::API_ENDPOINT, [
            'headers' => [
                'Content-type' => 'application/ld+json',
                'X-AUTH-TOKEN' => self::ADMIN_API_TOKEN,
            ],
            'json' => $newEvent,
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
    }
}