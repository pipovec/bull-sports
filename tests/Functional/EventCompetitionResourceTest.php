<?php declare(strict_types=1);


namespace App\Tests\Functional;


use App\Test\CustomApiTestCase;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use Symfony\Component\Uid\Uuid;

/**
 * @author Boris Fekar
 * @createdAt 30. 9. 2021
 * @package  App\Tests\Functional
 */
class EventCompetitionResourceTest extends CustomApiTestCase
{
    use ReloadDatabaseTrait;

    const API_ENDPOINT = '/api/events';

    public function testCreateTwoEventsCompetition()
    {
        $client = self::createClient();

        $userIRI = $this->createUser(
            'boris@admin.com',
            'Boris',
            'Admin',
            parent::ADMIN_PASSWORD,
            array('ROLE_USER','ROLE_ADMIN', 'ROLE_ORGANIZER'),
            parent::ADMIN_API_TOKEN
        );

        ### CREATE FIRS COMPETITION ###
        $newCompetition = $this->competition();
        $newCompetition['author'] = $userIRI;

        $result = $client->request("POST", '/api/competitions', [
            'headers' => [
                'Content-type' => 'application/ld+json',
                'X-AUTH-TOKEN' => self::ADMIN_API_TOKEN,
            ],
            'json' => $newCompetition,
        ]);

        $this->assertResponseStatusCodeSame(201);
        $result = json_decode($result->getContent(), true);
        $competitionIRI1 = $result['@id'];


        ### CREATE SECOND COMPETITION ###
        $newCompetition2 = $this->competition2();
        $newCompetition2['author'] = $userIRI;

        $result = $client->request("POST", '/api/competitions', [
            'headers' => [
                'Content-type' => 'application/ld+json',
                'X-AUTH-TOKEN' => self::ADMIN_API_TOKEN,
            ],
            'json' => $newCompetition2,
        ]);

        $this->assertResponseStatusCodeSame(201);
        $result = json_decode($result->getContent(), true);
        $competitionIRI2 = $result['@id'];
    }

    /**
     * @return string[]
     */
    private function competition(): array
    {
        return [
            "id" => Uuid::v4(),
            'name' => 'Test competition 1',
            'nameSk' => 'Testovacia disciplina 1',
            'description' => 'Testing competition 1',
            'imageName' => 'Test image',
        ];
    }

    /**
     * @return string[]
     */
    private function competition2(): array
    {
        return [
            "id" => Uuid::v4(),
            'name' => 'Test competition 2',
            'nameSk' => 'Testovacia disciplina 2',
            'description' => 'Testing competition 2',
            'imageName' => 'Test image 2',
        ];
    }

    private function discipline(): array
    {
       return  [
            "typeOfCompetition" => "",
            "start" => "2021-09-13T15:55:45.127Z",
            "ends" => "2021-09-13T17:55:45.127Z",
            "description" => "First competition"
        ];
    }

    private function discipline2(): array
    {
        return  [
            "typeOfCompetition" => "",
            "start" => "2021-09-13T15:55:45.127Z",
            "ends" => "2021-09-13T17:55:45.127Z",
            "description" => "Second competition"
        ];
    }
}