<?php declare(strict_types=1);


namespace App\Tests\Functional;


use App\Entity\User;
use App\Test\CustomApiTestCase;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Boris Fekar
 * @createdAt 3. 11. 2021
 * @package  App\Tests\Functional
 */
class UsersResourceTest extends CustomApiTestCase
{
    use ReloadDatabaseTrait;

    const API_ENDPOINT = '/api/users';

    public function testUnauthorizedCreateUser()
    {
        $client = self::createClient();

        $user = [
            'email' => 'test@test.com',
            'name' => 'Test user',
            'password' => 'test_password'
        ];

        $client->request('POST', self::API_ENDPOINT, [
            'headers' => [
                'Content-type' => 'application/json'
            ],
            'json' => $user,
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);
    }

    public function testAdminCanCreateUser()
    {
        $client = self::createClient();

        $userIRI = $this->createUser(
            'boris@admin.com',
            'Boris',
            'Admin',
            self::ADMIN_PASSWORD,
            ['ROLE_USER','ROLE_ADMIN'],
            self::ADMIN_API_TOKEN
        );

        $user = [
            'email' => 'test@test.com',
            'name' => 'Test user',
            'password' => 'test_password',
            'roles' => ["ROLE_USER", "ROLE_ADMIN"],
        ];

        $response = $client->request('POST', self::API_ENDPOINT, [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'X-AUTH-TOKEN' => self::ADMIN_API_TOKEN,
            ],
            'json' => $user,
        ]);
        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);

    }
}