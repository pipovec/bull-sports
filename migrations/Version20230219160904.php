<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230219160904 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE treadmill (id UUID NOT NULL, dog_id UUID NOT NULL, event_id UUID NOT NULL, result1 DOUBLE PRECISION DEFAULT NULL, result2 DOUBLE PRECISION DEFAULT NULL, result3 DOUBLE PRECISION DEFAULT NULL, result4 DOUBLE PRECISION DEFAULT NULL, avg_speed DOUBLE PRECISION DEFAULT NULL, max_speed DOUBLE PRECISION DEFAULT NULL, professional BOOLEAN DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D0A1A803634DFEB ON treadmill (dog_id)');
        $this->addSql('CREATE INDEX IDX_D0A1A80371F7E88B ON treadmill (event_id)');
        $this->addSql('COMMENT ON COLUMN treadmill.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN treadmill.dog_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN treadmill.event_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN treadmill.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE treadmill ADD CONSTRAINT FK_D0A1A803634DFEB FOREIGN KEY (dog_id) REFERENCES dogs (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE treadmill ADD CONSTRAINT FK_D0A1A80371F7E88B FOREIGN KEY (event_id) REFERENCES events (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE treadmill DROP CONSTRAINT FK_D0A1A803634DFEB');
        $this->addSql('ALTER TABLE treadmill DROP CONSTRAINT FK_D0A1A80371F7E88B');
        $this->addSql('DROP TABLE treadmill');
    }
}
