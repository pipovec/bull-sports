<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221226084558 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE aframe (id UUID NOT NULL, dog_id UUID NOT NULL, event_id UUID DEFAULT NULL, result1 DOUBLE PRECISION DEFAULT NULL, result2 DOUBLE PRECISION DEFAULT NULL, professional BOOLEAN DEFAULT NULL, dog_height DOUBLE PRECISION DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1B0B68C634DFEB ON aframe (dog_id)');
        $this->addSql('CREATE INDEX IDX_1B0B68C71F7E88B ON aframe (event_id)');
        $this->addSql('COMMENT ON COLUMN aframe.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN aframe.dog_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN aframe.event_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN aframe.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE breeds (id UUID NOT NULL, breed VARCHAR(50) NOT NULL, description TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN breeds.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE competition (id UUID NOT NULL, author_id UUID NOT NULL, image_id UUID DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, name_sk VARCHAR(255) DEFAULT NULL, description TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, seo_description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B50A2CB1F675F31B ON competition (author_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B50A2CB13DA5256D ON competition (image_id)');
        $this->addSql('COMMENT ON COLUMN competition.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN competition.author_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN competition.image_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN competition.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE dogs (id UUID NOT NULL, owner_id UUID DEFAULT NULL, breed_id UUID NOT NULL, image_id UUID DEFAULT NULL, name VARCHAR(255) NOT NULL, weight DOUBLE PRECISION NOT NULL, competition_weight DOUBLE PRECISION DEFAULT NULL, birthdate DATE NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_353BEEB37E3C61F9 ON dogs (owner_id)');
        $this->addSql('CREATE INDEX IDX_353BEEB3A8B4A30F ON dogs (breed_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_353BEEB33DA5256D ON dogs (image_id)');
        $this->addSql('COMMENT ON COLUMN dogs.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN dogs.owner_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN dogs.breed_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN dogs.image_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN dogs.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE events (id UUID NOT NULL, owner_id UUID NOT NULL, image_id UUID DEFAULT NULL, event_start TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, event_end TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, title VARCHAR(500) NOT NULL, place TEXT NOT NULL, description TEXT NOT NULL, is_published BOOLEAN NOT NULL, seo_description VARCHAR(255) DEFAULT NULL, workout BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5387574A7E3C61F9 ON events (owner_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5387574A3DA5256D ON events (image_id)');
        $this->addSql('COMMENT ON COLUMN events.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN events.owner_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN events.image_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE images (id UUID NOT NULL, awsname TEXT NOT NULL, name TEXT NOT NULL, mime_type TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN images.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE users (id UUID NOT NULL, email VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, surname VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, api_token VARCHAR(255) DEFAULT NULL, roles JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9E7927C74 ON users (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E97BA2F5EB ON users (api_token)');
        $this->addSql('COMMENT ON COLUMN users.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN users.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE aframe ADD CONSTRAINT FK_1B0B68C634DFEB FOREIGN KEY (dog_id) REFERENCES dogs (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE aframe ADD CONSTRAINT FK_1B0B68C71F7E88B FOREIGN KEY (event_id) REFERENCES events (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE competition ADD CONSTRAINT FK_B50A2CB1F675F31B FOREIGN KEY (author_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE competition ADD CONSTRAINT FK_B50A2CB13DA5256D FOREIGN KEY (image_id) REFERENCES images (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dogs ADD CONSTRAINT FK_353BEEB37E3C61F9 FOREIGN KEY (owner_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dogs ADD CONSTRAINT FK_353BEEB3A8B4A30F FOREIGN KEY (breed_id) REFERENCES breeds (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dogs ADD CONSTRAINT FK_353BEEB33DA5256D FOREIGN KEY (image_id) REFERENCES images (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE events ADD CONSTRAINT FK_5387574A7E3C61F9 FOREIGN KEY (owner_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE events ADD CONSTRAINT FK_5387574A3DA5256D FOREIGN KEY (image_id) REFERENCES images (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE aframe DROP CONSTRAINT FK_1B0B68C634DFEB');
        $this->addSql('ALTER TABLE aframe DROP CONSTRAINT FK_1B0B68C71F7E88B');
        $this->addSql('ALTER TABLE competition DROP CONSTRAINT FK_B50A2CB1F675F31B');
        $this->addSql('ALTER TABLE competition DROP CONSTRAINT FK_B50A2CB13DA5256D');
        $this->addSql('ALTER TABLE dogs DROP CONSTRAINT FK_353BEEB37E3C61F9');
        $this->addSql('ALTER TABLE dogs DROP CONSTRAINT FK_353BEEB3A8B4A30F');
        $this->addSql('ALTER TABLE dogs DROP CONSTRAINT FK_353BEEB33DA5256D');
        $this->addSql('ALTER TABLE events DROP CONSTRAINT FK_5387574A7E3C61F9');
        $this->addSql('ALTER TABLE events DROP CONSTRAINT FK_5387574A3DA5256D');
        $this->addSql('DROP TABLE aframe');
        $this->addSql('DROP TABLE breeds');
        $this->addSql('DROP TABLE competition');
        $this->addSql('DROP TABLE dogs');
        $this->addSql('DROP TABLE events');
        $this->addSql('DROP TABLE images');
        $this->addSql('DROP TABLE users');
    }
}
