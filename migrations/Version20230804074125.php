<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230804074125 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE aframe ADD result3 FLOAT');
        $this->addSql('ALTER TABLE aframe ADD result4 FLOAT');
        $this->addSql('ALTER TABLE aframe ADD result5 FLOAT');
        $this->addSql('ALTER TABLE aframe ADD result6 FLOAT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE aframe DROP result3');
        $this->addSql('ALTER TABLE aframe DROP result4');
        $this->addSql('ALTER TABLE aframe DROP result5');
        $this->addSql('ALTER TABLE aframe DROP result6');
    }
}
